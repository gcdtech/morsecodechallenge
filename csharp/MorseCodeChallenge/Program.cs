﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseCodeChallenge
{
    /**
     * The Morse Decoding class.
     * <p>
     * Write a method to decode morse code contained in a file.
     * <p>
     * The file contains morse code characters with spaces between letters and a / character between words.
     * <p>
     * For example given the string:
     * <p>
     * - --- / -... . / --- .-. / -. --- - / - --- / -... .
     * <p>
     * The output should be:
     * <p>
     * TO BE OR NOT TO BE
     */
    class Program
    {
        private Dictionary<String, String> morseCodes = new Dictionary<String, String>();

        public void setupMorseCodesHashMap()
        {
            morseCodes.Add("A", ".-");
            morseCodes.Add("B", "-...");
            morseCodes.Add("C", "-.-.");
            morseCodes.Add("D", "-..");
            morseCodes.Add("E", ".");
            morseCodes.Add("F", "..-.");
            morseCodes.Add("G", "--.");
            morseCodes.Add("H", "....");
            morseCodes.Add("I", "..");
            morseCodes.Add("J", ".---");
            morseCodes.Add("K", "-.-");
            morseCodes.Add("L", ".-..");
            morseCodes.Add("M", "--");
            morseCodes.Add("N", "-.");
            morseCodes.Add("O", "---");
            morseCodes.Add("P", ".--.");
            morseCodes.Add("Q", "--.-");
            morseCodes.Add("R", ".-.");
            morseCodes.Add("S", "...");
            morseCodes.Add("T", "-");
            morseCodes.Add("U", "..-");
            morseCodes.Add("V", "...-");
            morseCodes.Add("W", ".--");
            morseCodes.Add("X", "-..-");
            morseCodes.Add("Y", "-.--");
            morseCodes.Add("Z", "--..");

            morseCodes.Add("0", "-----");
            morseCodes.Add("1", ".----");
            morseCodes.Add("2", "..---");
            morseCodes.Add("3", "...--");
            morseCodes.Add("4", "....-");
            morseCodes.Add("5", ".....");
            morseCodes.Add("6", "-....");
            morseCodes.Add("7", "--...");
            morseCodes.Add("8", "---..");
            morseCodes.Add("9", "----.");

            morseCodes.Add(".", ".-.-.-");
            morseCodes.Add(",", "--..--");
            morseCodes.Add("?", "..--..");
            morseCodes.Add("=", "-...-");
        }

        public String decode(String filePath)
        {
            String decodedText = "";

            // **************************************************************************************
            // Write your algorithm here to convert the content in the file filePath to decoded text.
            // Return the decoded text as the result of this function.
            // **************************************************************************************



            return decodedText;
        }

        static void Main(string[] args)
        {
            Program morseDecoder = new Program();
            morseDecoder.setupMorseCodesHashMap();

            Console.WriteLine(morseDecoder.decode(@"..\..\..\morse.txt"));

            System.Console.WriteLine("Press any key to exit");
            System.Console.ReadKey();
        }
    }
}
