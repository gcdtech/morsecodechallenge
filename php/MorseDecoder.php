<?php

/**
 * The Morse Decoding class.
 *
 * Write a function to decode morse code contained in a file.
 *
 * The file contains morse code characters with spaces between letters and a / character between words.
 *
 * For example given the string:
 *
 * - --- / -... . / --- .-. / -. --- - / - --- / -... .
 *
 * The output should be:
 *
 * TO BE OR NOT TO BE
 */
class MorseDecoder
{
    private $morseCodes = [
        "A" => ".-",
        "B" => "-...",

        "C" => "-.-.",
        "D" => "-..",
        "E" => ".",
        "F" => "..-.",
        "G" => "--.",
        "H" => "....",
        "I" => "..",
        "J" => ".---",
        "K" => "-.-",
        "L" => ".-..",
        "M" => "--",
        "N" => "-.",
        "O" => "---",
        "P" => ".--.",
        "Q" => "--.-",
        "R" => ".-.",
        "S" => "...",
        "T" => "-",
        "U" => "..-",
        "V" => "...-",
        "W" => ".--",
        "X" => "-..-",
        "Y" => "-.--",
        "Z" => "--..",

        "0" => "-----",
        "1" => ".----",
        "2" => "..---",
        "3" => "...--",
        "4" => "....-",
        "5" => ".....",
        "6" => "-....",
        "7" => "--...",
        "8" => "---..",
        "9" => "----.",

        "." => ".-.-.-",
        "," => "--..--",
        "?" => "..--..",
        "=" => "-...-",

    ];

    public function decode($filePath)
    {
        $decodedText = "";

        // Write your algorithm here to convert the content in the file $filePath to decoded text.
        // Return the decoded text as the result of this function.


        return $decodedText;
    }
}

$decoder = new MorseDecoder();
print $decoder->decode(__DIR__."/morse.txt");