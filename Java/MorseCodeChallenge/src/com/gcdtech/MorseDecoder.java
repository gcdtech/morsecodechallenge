package com.gcdtech;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * The Morse Decoding class.
 * <p>
 * Write a method to decode morse code contained in a file.
 * <p>
 * The file contains morse code characters with spaces between letters and a / character between words.
 * <p>
 * For example given the string:
 * <p>
 * - --- / -... . / --- .-. / -. --- - / - --- / -... .
 * <p>
 * The output should be:
 * <p>
 * TO BE OR NOT TO BE
 */
public class MorseDecoder {

    private HashMap<String, String> morseCodes = new HashMap<String, String>();

    public void setupMorseCodesHashMap() {
        morseCodes.put("A", ".-");
        morseCodes.put("B", "-...");
        morseCodes.put("C", "-.-.");
        morseCodes.put("D", "-..");
        morseCodes.put("E", ".");
        morseCodes.put("F", "..-.");
        morseCodes.put("G", "--.");
        morseCodes.put("H", "....");
        morseCodes.put("I", "..");
        morseCodes.put("J", ".---");
        morseCodes.put("K", "-.-");
        morseCodes.put("L", ".-..");
        morseCodes.put("M", "--");
        morseCodes.put("N", "-.");
        morseCodes.put("O", "---");
        morseCodes.put("P", ".--.");
        morseCodes.put("Q", "--.-");
        morseCodes.put("R", ".-.");
        morseCodes.put("S", "...");
        morseCodes.put("T", "-");
        morseCodes.put("U", "..-");
        morseCodes.put("V", "...-");
        morseCodes.put("W", ".--");
        morseCodes.put("X", "-..-");
        morseCodes.put("Y", "-.--");
        morseCodes.put("Z", "--..");

        morseCodes.put("0", "-----");
        morseCodes.put("1", ".----");
        morseCodes.put("2", "..---");
        morseCodes.put("3", "...--");
        morseCodes.put("4", "....-");
        morseCodes.put("5", ".....");
        morseCodes.put("6", "-....");
        morseCodes.put("7", "--...");
        morseCodes.put("8", "---..");
        morseCodes.put("9", "----.");

        morseCodes.put(".", ".-.-.-");
        morseCodes.put(",", "--..--");
        morseCodes.put("?", "..--..");
        morseCodes.put("=", "-...-");
    }

    public String decode(String filePath) {
        String decodedText = "";

        // **************************************************************************************
        // Write your algorithm here to convert the content in the file filePath to decoded text.
        // Return the decoded text as the result of this function.
        // **************************************************************************************



        return decodedText;
    }

    public static void main(String[] args) {
        MorseDecoder morseDecoder = new MorseDecoder();
        morseDecoder.setupMorseCodesHashMap();
        System.out.println(morseDecoder.decode("../../morse.txt"));
    }
}